
Hooks.once('init', async function () {
    // 註冊設定頁面
    let modulename = "arwai-welcome-sound";

    // 設定選項

    // 打開聲音
    game.settings.register(modulename,
        "welcome-sound-enable",
        {
            name: i18n("WelcomeSound.welcomeSoundEnable.name"),
            hint: i18n("WelcomeSound.welcomeSoundEnable.hint"),
            scope: "world",
            config: true,
            default: true,
            type: Boolean
        });

    // 檔案路徑
    game.settings.register(modulename,
        "welcome-sound-file",
        {
            name: i18n("WelcomeSound.welcomeSoundFile.name"),
            hint: i18n("WelcomeSound.welcomeSoundFile.hint"),
            scope: "world",
            config: true,
            default: "modules/arwai-welcome-sound/sound/Tomoka_HanazawaKana.ogg",
            type: String,
            filePicker: 'audio'
        });

    // 隨機音效檔選項
    game.settings.register(modulename,
        "welcome-sound-random-file",
        {
            name: i18n("WelcomeSound.welcomeSoundRandomFile.name"),
            hint: i18n("WelcomeSound.welcomeSoundRandomFile.hint"),
            scope: "world",
            config: true,
            default: false,
            type: Boolean
        });

    // 隨機資料夾路徑
    game.settings.register(modulename,
        "welcome-sound-folder",
        {
            name: i18n("WelcomeSound.welcomeSoundFolder.name"),
            hint: i18n("WelcomeSound.welcomeSoundFolder.hint"),
            scope: "world",
            config: true,
            default: "",
            type: String,
            filePicker: 'folder'
        });

    // 音量
    game.settings.register(modulename,
        "welcome-sound-volumne",
        {
            name: i18n("WelcomeSound.welcomeSoundVolumne.name"),
            hint: i18n("WelcomeSound.welcomeSoundVolumne.hint"),
            scope: "world",
            config: true,
            default: 60,
            type: Number,
            range: {
                min: 0,
                max: 100,
                step: 1
            }
        });

    // 對玩家開放
    game.settings.register(modulename,
        "welcome-sound-enable-player",
        {
            name: i18n("WelcomeSound.welcomeSoundEnableForPlayers.name"),
            hint: i18n("WelcomeSound.welcomeSoundEnableForPlayers.hint"),
            scope: "world",
            config: true,
            default: false,
            type: Boolean,
        });

    // 歡迎文字
    game.settings.register(modulename,
        "welcome-text",
        {
            name: i18n("WelcomeSound.welcomeText.name"),
            hint: i18n("WelcomeSound.welcomeText.hint"),
            scope: "world",
            config: true,
            default: "Welcome!",
            type: String,
        });
});

// 播出Welcome音效檔案
Hooks.on("ready", function () {
    console.log("arWai Welcome sound | ", i18n("WelcomeSound.welcomeTip"));

    // 判定是否關閉 或者 判定是否GM
    if (!setting("welcome-sound-enable") || !setting("welcome-sound-enable-player") && !game.user.isGM)
        return;

    ui.notifications.info(setting("welcome-text"));
    playWelcomeSounds();
});

// 播放音效
async function playWelcomeSounds() {
    const isRandom = setting("welcome-sound-random-file");
    let audioFile = null;
    if (!isRandom)
        audioFile = setting("welcome-sound-file");
    else {
        let audioFolder = setting("welcome-sound-folder");
        let folder = await getFolder(audioFolder);
        if (folder && folder.files) {
            let files = folder.files;
            audioFile = files[Math.floor(Math.random() * files.length)];
        }
    }

    if (audioFile.length > 0) {
        let volume = (setting('welcome-sound-volumne') / 100);
        await AudioHelper.play({ src: audioFile, volume: volume });
        console.log("arWai Welcome sound | Playing Sound: ", audioFile);
    }
    return;
}

// 抓i18n的語言檔
let i18n = key => {
    return game.i18n.localize(key);
};

// 抓設定
let setting = key => {
    return game.settings.get("arwai-welcome-sound", key);
};

/**
 * Creates folders recursively (much better than previous
 */
async function getFolder(path) {
    let parentFolder = await FilePicker.browse("data", path);
    return parentFolder;
}